package com.example.cryptoappinformation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.cryptoappinformation.databinding.FragmentInfoBinding
import com.example.cryptoappinformation.databinding.FragmentMenuBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentInfo : Fragment() {

    private lateinit var binding: FragmentInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInfoBinding.inflate(inflater, container, false)

        parentFragmentManager.setFragmentResultListener("User", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                val result = result.getParcelable<User>("User")
                if (result != null) {
                    binding.titulo.text = "${result.name}"
                    binding.symbol.text = "${result.symbol}"
                    binding.marketcap.text = "Market Cap: ${result.marketcap}"
                    binding.price.text = "Price: ${result.price}"
                    Glide.with(this)
                        .load(result?.url)
                        .placeholder(R.drawable.logo)
                        .into(binding.imageIcon)
                }
            })
        binding.btrecyclev.setOnClickListener {
            val action = FragmentInfoDirections.actionFragmentInfo3ToFragment12()
            findNavController().navigate(action)

        }

            return binding.root
        }


}