package com.example.cryptoappinformation


import com.example.cryptoappinformation.Data.Data
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {
    //Aquí posem les operacions GET,POST, PUT i DELETE vistes abans
    companion object {
        val BASE_URL = "https://api.coingecko.com/api/v3/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }


    }
    @GET("search/trending/")
    fun getData(): Call<Data>
}
