package com.example.cryptoappinformation


import android.util.Log
import com.example.cryptoappinformation.Data.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private val apiInterface = ApiInterface.create()

    fun GetCrypto() {
        val call = apiInterface.getData()
        call.enqueue(object: Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Data?>, response: Response<Data?>) {
                if (response != null && response.isSuccessful) {
                    val myData = response.body()
                    if (myData != null) {
                        println(myData.coins)
                    }
                }
            }
        })


    }
}