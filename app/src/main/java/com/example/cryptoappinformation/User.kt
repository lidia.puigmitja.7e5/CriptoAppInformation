package com.example.cryptoappinformation

import android.os.Parcel
import android.os.Parcelable
import com.example.cryptoappinformation.User.Companion.write
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(val id: Long, var name: String?, var symbol: String?,var marketcap: Double,var price:Double,var url: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString()
    ) {
    }

    companion object : Parceler<User> {

        override fun User.write(parcel: Parcel, flags: Int) {
            parcel.writeLong(id)
            parcel.writeString(name)
            parcel.writeString(symbol)
            parcel.writeDouble(marketcap)
            parcel.writeDouble(price)
            parcel.writeString(url)
        }

        override fun create(parcel: Parcel): User {
            return User(parcel)
        }
    }
}