package com.example.cryptoappinformation.Data

data class Data(
    val coins: List<Coin>,
    val exchanges: List<Any>
)