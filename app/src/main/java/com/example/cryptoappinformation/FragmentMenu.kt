package com.example.cryptoappinformation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.cryptoappinformation.databinding.FragmentMenuBinding


class FragmentMenu : Fragment() {

    private lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(inflater,container, false)

        val switch= binding.switchMode
        val imMode= binding.imMode

        val prefs = requireActivity().getSharedPreferences("pref", Context.MODE_PRIVATE)
        val editor=prefs.edit()
        val nightMODE = prefs.getBoolean("night", false)
        val switchMode=prefs.getBoolean("switch", false)

        if(nightMODE){
            switch.isChecked=true
            imMode.setImageResource(R.drawable.ic_dark)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            editor.putBoolean("switch", true)
            switch.text="Desactivar modo oscuro: "
        }else{
            switch.isChecked=false
            imMode.setImageResource(R.drawable.ic_light)
            switch.text="Activar modo oscuro: "
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            editor.putBoolean("switch", false)
        }

        switch.setOnCheckedChangeListener(){_,_->
            if(switch.isChecked){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                switch.text="Desactivar modo oscuro: "
                imMode.setImageResource(R.drawable.ic_dark)
                editor.putBoolean("night", true)
                editor.putBoolean("switch", true)
            }else{
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                switch.text="Activar modo oscuro: "
                imMode.setImageResource(R.drawable.ic_light)
                editor.putBoolean("night", false)
                editor.putBoolean("switch", false)
            }
            editor.apply()
        }


        binding.start.setOnClickListener(){
            val action = FragmentMenuDirections.actionFragmentMenu2ToFragment12()
            findNavController().navigate(action)

        }
        return binding.root
    }

}